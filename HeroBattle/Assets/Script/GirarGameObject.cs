﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GirarGameObject : MonoBehaviour {

    public float speed;
    // Update is called once per frame
    void Update()
    {
        float offset = Time.deltaTime * speed;

        transform.Rotate(Vector3.up, offset * .8f);
        transform.Rotate(Vector3.left, offset);

    }
}
