﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MagiaColisao : MonoBehaviour {

    string nivel_ataque;

    void OnTriggerEnter(Collider obj)
    {
        if (gameObject.tag == "Hero" && obj.name.Contains("Heroi"))
            return;

        if (gameObject.tag == "Enemy" && obj.name.Contains("Inimigo"))
            return;

        VerificaColidido(obj.tag);
        Destroy(obj.gameObject);

    }

    void OnParticleCollision(GameObject obj)
    {
        VerificaColidido(obj.tag);
    }

    private void VerificaColidido(string tag)
    {
        nivel_ataque = tag;

        switch (gameObject.tag)
        {
            case "Alied":
                EscudoAtingido();
                break;

            case "Defense":
                EscudoAtingido();
                break;

            default: Colisao(); break;
        }
    }

    private void EscudoAtingido()
    {
    }

    private void Colisao()
    {
        var player = gameObject.GetComponentInChildren<BaseStatus>(); 
        float dano = 0;

        switch (nivel_ataque)
        {
            case "Atack_1": dano = (float)Game.DanoAtaque.BASICO; break;
            case "Atack_2": dano = (float)Game.DanoAtaque.MEDIO; break;
            case "Atack_3": dano = (float)Game.DanoAtaque.ESPECIAL; break;
        }

        player.vidaAtual -= dano;

        float VidaRestante = player.vidaAtual / player.vidaBase;
        float barraW = (player.vidaBase - player.vidaAtual) * 2;

        GameObject barra;

        if (gameObject.tag == "Hero")
        {
            barra = GameObject.Find("HP_HEROI");
            ((Animator)GameObject.Find("Heroi").GetComponent("Animator")).SetTrigger("Damage");
        }
        else
        {
            barra = GameObject.Find("HP_INIMIGO");
            ((Animator)GameObject.Find("Inimigo").GetComponent("Animator")).SetTrigger("Damage");
        }

        barra.GetComponent<RectTransform>().sizeDelta = new Vector2(barraW, 80);

        MostrarDano(dano);

        if (VidaRestante < 0) {
            GameManager.heroWin = (gameObject.tag == "Enemy");
            ((Animator)GameObject.Find("Camera").GetComponent("Animator")).SetTrigger("GameEnd");

            string msgWin = string.Empty;

            if (GameManager.heroWin)
            {
                msgWin = "PARABENS!\n Você Venceu!";
                ((Animator)GameObject.Find("Inimigo").GetComponent("Animator")).SetTrigger("Dead");
            }
            else
            {
                msgWin = "PERDEU!\n Inimigo Venceu!";
                ((Animator)GameObject.Find("Heroi").GetComponent("Animator")).SetTrigger("Dead");
            }

            GameManager.CriaTextSplash(msgWin, true);
            GameManager.isGameEnd = true;

        }

        GameManager.heroTurn = !GameManager.heroTurn;
    }

    private void MostrarDano(float dano)
    {
        GameObject original = GameObject.Find("TextFloat");
        GameObject canvas = GameObject.Find("Canvas");

        GameObject copia = (GameObject)Instantiate(original);
        copia.transform.SetParent(canvas.transform);

        Vector2 pos = gameObject.transform.position;  // get the game object position
        Vector2 viewportPoint = Camera.main.WorldToScreenPoint(pos);  //convert game object position to VievportPoint

        float variacaoX = (gameObject.tag == "Hero") ? Random.Range(-140, -100) : Random.Range(100, 140);
        float variacaoY = Random.Range(70, 100);

        Vector2 novaPos = new Vector2(viewportPoint.x - variacaoX, viewportPoint.y + variacaoY);

        copia.GetComponent<RectTransform>().transform.position = novaPos;
        copia.GetComponentInChildren<Text>().text = string.Format("-{0}", dano.ToString());
        Destroy(copia, 3);
    }

}
