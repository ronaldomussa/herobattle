﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComandoScript : MonoBehaviour {

    public float Speed = 50;

    public GameObject Heroi_Ataque_1;
    public GameObject Heroi_Ataque_2;
    public GameObject Heroi_Ataque_3;
    public GameObject Heroi_Defesa;
    public GameObject Heroi_Aliado;

    public GameObject Inimigo_Ataque_1;
    public GameObject Inimigo_Ataque_2;
    public GameObject Inimigo_Ataque_3;
    public GameObject Inimigo_Defesa;
    public GameObject Inimigo_Aliado;
    
    #region ATAQUES
    public void Atacar()
    {
        if (GameManager.heroTurn)
            ((Animator)GameObject.Find("Heroi").GetComponent("Animator")).SetTrigger("Atack");
        else
            ((Animator)GameObject.Find("Inimigo").GetComponent("Animator")).SetTrigger("Atack");

        // faz a roleta
        int random = Random.Range(0, 6);

        if (random == 0 || random == 1 || random == 2)
            CopiarAtaque(GameManager.heroTurn ? Heroi_Ataque_1 : Inimigo_Ataque_1);
        if (random == 3 || random == 4)
            CopiarAtaque(GameManager.heroTurn ? Heroi_Ataque_2 : Inimigo_Ataque_2);
        if (random == 5) 
            AtaqueEspecial();       

        

    }

    private void AtaqueEspecial()
    {
        ParticleSystem ataqueEspecial = new ParticleSystem();

        if(GameManager.heroTurn)
            ataqueEspecial = Heroi_Ataque_3.GetComponent<ParticleSystem>();
        else
            ataqueEspecial = Inimigo_Ataque_3.GetComponent<ParticleSystem>();

        var en = ataqueEspecial.emission;
        en.enabled = true;

        ataqueEspecial.Play();
    }

    private void CopiarAtaque(GameObject ataque)
    {
        GameObject copia;
        copia = (GameObject)Instantiate(ataque);
        copia.AddComponent<MagiaScript>();
    }
   
    #endregion

    public void Defender()
    {

        // faz a roleta
        int random = Random.Range(0, 6);

        if (random == 0 || random == 1 || random == 2)
            DefesaBasica();
        if (random == 3 || random == 4)
            DefesaMedia();
        if (random == 5)
            DefesaEspecial();

    }

    private void DefesaEspecial()
    {

    }

    private void DefesaMedia()
    {
    }

    private void DefesaBasica()
    {
    }

    public void Aliado()
    {


    }
    
}
