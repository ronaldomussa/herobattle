﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour {

    public enum TipoAtaqueHeroi
    {
        BOLA_DE_FOGO = 1,
        RAJADA_DE_FOGO = 2,
        CHUVA_DE_FOGO = 3
    };

    public enum TipoAtaqueInimigo
    {
        GOSMA = 1,
        RAJADA_DE_GOSMA = 2,
        CHUVA_DE_GOSMA = 3
    };

    public enum DanoAtaque
    {
        BASICO = 30,
        MEDIO = 50,
        ESPECIAL = 6 //  cada particula
    };

    public enum Defesa
    {
        BASICO = 50,
        MEDIO = 100,
        ESPECIAL = 180
    }


}
