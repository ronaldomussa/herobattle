﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagiaScript : MonoBehaviour {

    public float speed = 7;

    void Start()
    {
        // posicao inicial da magia de acordo com o turno

        if(GameManager.heroTurn)
            gameObject.transform.position = GameManager.posicaoHero;
        else
            gameObject.transform.position = GameManager.posicaoEnemy;
    }
	
	void Update () {
        float step = speed * Time.deltaTime;

        // target da magia de acordo com o turno

        if (GameManager.heroTurn)
            gameObject.transform.position = Vector3.MoveTowards(transform.position, GameManager.posicaoEnemy, step);
        else
            gameObject.transform.position = Vector3.MoveTowards(transform.position, GameManager.posicaoHero, step);
    }


}
