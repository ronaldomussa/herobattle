﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonMenu : MonoBehaviour {

    public void PlayBtn(int scene)
    {
        //((Animator)GameObject.Find("Camera").GetComponent("Animator")).SetTrigger("Finaliza");
        ((AudioSource)GameObject.Find("btn_play").GetComponent("AudioSource")).Play();
        StartCoroutine(GoDelay());
        //SceneManager.LoadScene(scene);
    }

    public IEnumerator GoDelay()
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("Batalha");
    }

    public void ExitGameBtn()
    {
        Application.Quit();
    }
}
