﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BaseStatus : MonoBehaviour
{
    public string nome;
    public float vidaBase;
    public float vidaAtual;

    public int danoBase;
    public int danoAtual;

    public int defesaBase;
    public int defesaAtual;

    public float xp;
    private float nextLevel;
    public int level;

}
