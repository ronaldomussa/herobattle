﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public static Vector3 posicaoHero;
    public static Vector3 posicaoEnemy;
    public static Transform transformHeroi;
    public static Transform transformInimigo;
    public static bool heroTurn = true;
    public static bool isGameEnd = false;
    public static bool heroWin = true;

    void Start()
    {
        PosicaoPlayers();

        // randomiza quem começa o turno
        int zeroUm = Random.Range(0, 2);
        //int zeroUm = 1;

        if (zeroUm == 0)
        {
            heroTurn = true;
            CriaTextSplash("Você começa!", true);
        }
        else
        {
            heroTurn = false;
            CriaTextSplash("Inimigo começa!", true);
            StartCoroutine(InimigoAtacarDelay());
            
        }


    }

    public IEnumerator InimigoAtacarDelay()
    {
        yield return new WaitForSeconds(3);
        ((ComandoScript)GameObject.Find("GameManager").GetComponent("ComandoScript")).Atacar();

    }

    private void PosicaoPlayers()
    {
        // instanciando variaves publicas do Hero e Enemy
        // estas variaveis podem ser acessadas fora desta class

        var hero = GameObject.FindGameObjectWithTag("Hero");
        var enemy = GameObject.FindGameObjectWithTag("Enemy");

        transformHeroi = hero.transform;
        transformInimigo = enemy.transform;

        posicaoHero = new Vector3(transformHeroi.position.x, .5f, transformHeroi.position.z);
        posicaoEnemy = new Vector3(transformInimigo.position.x, .5f, transformInimigo.position.z);
    }

    public static void CriaTextSplash(string text, bool destaque)
    {
        GameObject original = GameObject.Find("TextSplash");
        GameObject canvas = GameObject.Find("Canvas");

        GameObject copia = (GameObject)Instantiate(original);
        copia.transform.SetParent(canvas.transform);

        copia.GetComponent<RectTransform>().transform.position = new Vector3(0, 55);
        copia.GetComponent<Text>().text = text;

        if (!destaque)
        {
            copia.GetComponent<Text>().fontSize = 20;
            copia.GetComponent<Text>().resizeTextForBestFit = false;
        }

        Destroy(copia, 5);

    }

    void Update()
    {
        if(isGameEnd)
        {
            ((CanvasGroup)GameObject.Find("panel_acao").GetComponent("CanvasGroup")).interactable = false;
        }

        if (!isGameEnd) { 
            if (heroTurn)
                GameObject.Find("IndicaTurno").transform.position = new Vector3(posicaoHero.x, 0.05f, posicaoHero.z);
            else
                GameObject.Find("IndicaTurno").transform.position = new Vector3(posicaoEnemy.x, 0.05f, posicaoEnemy.z);
        }

    }
}
