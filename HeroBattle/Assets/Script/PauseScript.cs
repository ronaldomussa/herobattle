﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseScript : MonoBehaviour {

    bool isPaused = false;

    public void Pausar () {

        GameObject MusicaFundo = GameObject.Find("MusicaDeBatalha");

        if (Time.timeScale == 0)
        {
            MusicaFundo.GetComponent<AudioSource>().Play();

            // RESTART
            // SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

            Time.timeScale = 1;
        }
        else
        {
            Time.timeScale = 0;
            MusicaFundo.GetComponent<AudioSource>().Pause();
            isPaused = true;
        }

    }
	
}
